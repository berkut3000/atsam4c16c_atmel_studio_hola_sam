/*
 * delay_conf.c
 *
 * Created: 24/01/2018 08:22:40 a.m.
 *  Author: hydro
 */ 

#include "delay_conf.h"
//#include "stdio.h"

void Delay_N_ms(uint16_t val)
{
	uint16_t j;
	for(j=0; j<val; j++)
	{
		Delay_N_us(1330);
	}
}

void Delay_N_us(uint16_t val)
{
	volatile uint32_t f;
	uint32_t j,k;
	for(j=0;j<val;j++)
	{
		f=16000000;
		asm("nop");
		asm("nop");
		asm("nop");
		for(k=0;k<(f/30000000)+1;k++)
		{
			asm("nop");
		}
		
	}
}